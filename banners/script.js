const images = [...document.querySelectorAll(".image-to-show")];
const start = document.querySelector(".start-slider");
const stop = document.querySelector(".stop-slider");

let count = 1;
let runSlider;

function slideImages(n = count){
    runSlider = setInterval(() => {
        if (n === images.length) {
            n = 0;
        }
        if (n === 0) {
          images[images.length - 1].classList.add("hidden");
        } else {
          images[n - 1].classList.add("hidden");
        }
        images[n].classList.remove("hidden");      
        n++;
        count = n;
      }, 3000);
}
slideImages()

stop.addEventListener("click", () => {  
  clearInterval(runSlider);
});

start.addEventListener("click", () => {
    slideImages(count);  
});
